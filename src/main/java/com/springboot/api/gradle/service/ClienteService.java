package com.springboot.api.gradle.service;

import java.util.Map;

import com.springboot.api.gradle.model.Cliente;

public interface ClienteService {
	
	Map<String, Object> getAllClientes();
	Map<String, Object> getCliente(Integer id);
	Map<String, Object> saveCliente(Cliente cliente);
	Map<String, Object> deleteCliente(Integer id);
	
}
