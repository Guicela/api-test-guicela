package com.springboot.api.gradle.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.gradle.dao.impl.ClienteDaoImpl;
import com.springboot.api.gradle.model.Cliente;
import com.springboot.api.gradle.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ClienteDaoImpl clienteDao;
	
	@Override
	public Map<String, Object> getAllClientes() {
		Map<String, Object> rpta = new HashMap<String, Object>();
		rpta.put("codigo_servicio", "0000");
		rpta.put("clientes", clienteDao.getAllClientes());
		return rpta;
	}

	@Override
	public Map<String, Object> getCliente(Integer id) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		rpta.put("codigo_servicio", "0000");
		rpta.put("cliente", clienteDao.getCliente(id));
		return rpta;
	}

	@Override
	public Map<String, Object> saveCliente(Cliente cliente) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		
		try {
			clienteDao.saveCliente(cliente);
			rpta.put("codigo_servicio", "0000");
			rpta.put("descripcion", "Cliente registrado con éxito");
		} catch (Exception e) {
			logger.error(e.getMessage());
			rpta.put("codigo_servicio", "0001");
			rpta.put("descripcion", "Error al registrar cliente");
		}	
		return rpta;
	}

	@Override
	public Map<String, Object> deleteCliente(Integer id) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		try {
			clienteDao.deleteCliente(id);
			rpta.put("codigo_servicio", "0000");
			rpta.put("descripcion", "Cliente eliminado con éxito");
		} catch (Exception e) {
			logger.error(e.getMessage());
			rpta.put("codigo_servicio", "0001");
			rpta.put("descripcion", "Error al eliminar cliente");
		}
		return rpta;
	}

}
