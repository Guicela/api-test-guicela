package com.springboot.api.gradle.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.gradle.model.Cliente;
import com.springboot.api.gradle.service.impl.ClienteServiceImpl;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteServiceImpl clienteService;
	
	@GetMapping(value = "", produces = "application/json")	
	public Map<String, Object> getAllClientes(){
		return clienteService.getAllClientes();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")	
	public Map<String, Object> getCliente(@PathVariable ("id") Integer id){
		return clienteService.getCliente(id);
	}
	
	@PostMapping(value = "", produces = "application/json")	
	public Map<String, Object> saveCliente(@RequestBody Cliente cliente){
		return clienteService.saveCliente(cliente);
	}	
	
	@DeleteMapping(value = "/{id}", produces = "application/json")	
	public Map<String, Object> deleteCliente(@PathVariable ("id") Integer id){
		return clienteService.deleteCliente(id);
	}	

}
